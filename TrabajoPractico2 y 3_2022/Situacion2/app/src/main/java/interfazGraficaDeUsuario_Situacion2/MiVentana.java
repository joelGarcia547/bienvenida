package interfazGraficaDeUsuario_Situacion2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MiVentana extends JFrame{
    JDesktopPane escritorio;
    public JMenuBar barra; 
    public JMenu _producto;
    public JMenu _pedido;
    public JMenuItem _agregar;
    public JMenuItem _buscar;
    public JMenuItem _remover;

    public MiVentana(String _titulo, String _contactoDeLocal, Integer x, Integer y, Integer ancho, Integer alto){
        super(_titulo);
        this.setBounds(x, y, ancho, alto);

        escritorio = new JDesktopPane();
        barra = new JMenuBar();

        _producto = new JMenu("Productos");
        _pedido = new JMenu("Pedidos");

        _agregar = new JMenuItem("Agregar Pedido");

        _buscar = new JMenuItem("Buscar Pedido");

        _remover = new JMenuItem("Eliminar Pedido");

        //agregar ActionListener mediante clase anonima
        _agregar.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
               NuevaVentanaAgregarPedido _agregarPedido = new NuevaVentanaAgregarPedido();  
               escritorio.add(_agregarPedido);
               _agregarPedido.setVisible(true);
            }

        }); 


        //agregar el panel al framework
        this.add(barra);
        barra.add(_producto);
        barra.add(_pedido);
        _producto.add(_agregar);
        _producto.add(_buscar);
        _producto.add(_remover);

        //asociar el objeto JMenuBar al JFrame
        setContentPane(escritorio);
        setJMenuBar(barra);

        
        //para que se muestre el panel
        setVisible(true);
    }
}
