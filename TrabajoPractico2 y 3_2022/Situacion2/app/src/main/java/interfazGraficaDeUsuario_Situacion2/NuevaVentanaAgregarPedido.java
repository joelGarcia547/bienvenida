package interfazGraficaDeUsuario_Situacion2;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class NuevaVentanaAgregarPedido extends JFrame{
    public JFrame panel;
    //producto
    public JLabel lblCodigoID;
    public JTextField txtCodigo;
    public JLabel lblDescripcion;
    public JTextField txtDescripcion;
    public JLabel lblPrecio;
    public JTextField txtPrecio;
    public JLabel lblExistencia;
    public JTextField txtStock;
    public JLabel lblOrigen;
    public JTextField txtOrigen;

    //cantidad del producto
    public JLabel lblCantidadDelProducto;
    public JTextField txtCantidadDeProducto;

    //botones
    public JButton btnAgregarALista;
    public JButton btnCancelar;

    public NuevaVentanaAgregarPedido(){
        //String titulo, Integer x, Integer y, Integer ancho, Integer alto
        super("Nueva ventana");
        //setBounds(x, y, ancho, alto);

        panel = new JFrame();

        lblCantidadDelProducto = new JLabel("Cantidad del Producto:");
        txtCantidadDeProducto = new JTextField(" ");
        lblCodigoID = new JLabel("Codigo ID:");
        txtCodigo = new JTextField(" ");
        lblDescripcion = new JLabel("Descripcion:");
        txtDescripcion = new JTextField(" ");
        lblPrecio = new JLabel("Precio:");
        txtPrecio = new JTextField("$ ");
        lblExistencia = new JLabel("Stock:");
        txtStock = new JTextField(" ");
        lblOrigen = new JLabel("Origen:");
        txtOrigen = new JTextField(" ");
        
        panel.add(lblCantidadDelProducto);
        panel.add(txtCantidadDeProducto);
        panel.add(lblCodigoID);
        panel.add(txtCodigo);
        panel.add(lblDescripcion);
        panel.add(txtDescripcion);
        panel.add(lblPrecio);
        panel.add(txtPrecio);
        panel.add(lblExistencia);
        panel.add(txtStock);
        panel.add(lblOrigen);
        panel.add(txtOrigen);

        this.add(panel);
    }
}
