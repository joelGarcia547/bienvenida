package Exceptions;

public class PedidoInexistenteEnListaException extends RuntimeException{
    public PedidoInexistenteEnListaException(){
        super("El pedido es inexistente en el registro de pedidos");
    }
}
