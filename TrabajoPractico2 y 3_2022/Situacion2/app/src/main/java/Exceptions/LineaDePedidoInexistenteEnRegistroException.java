package Exceptions;

public class LineaDePedidoInexistenteEnRegistroException extends RuntimeException{
    public LineaDePedidoInexistenteEnRegistroException(){
        super("La linea de pedido no existe en el registro");
    }
}
