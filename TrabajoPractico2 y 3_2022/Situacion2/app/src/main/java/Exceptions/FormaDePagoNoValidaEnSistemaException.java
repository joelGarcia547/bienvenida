package Exceptions;

public class FormaDePagoNoValidaEnSistemaException extends RuntimeException{
    public FormaDePagoNoValidaEnSistemaException(){
        super("La forma de pago implementada no es valida en el sistema");
    }
}
