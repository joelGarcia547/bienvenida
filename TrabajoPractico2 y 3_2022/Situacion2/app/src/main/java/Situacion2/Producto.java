package Situacion2;

public class Producto {
    private String codigoID;
    private String descripcionDelProducto;
    private Float precioDelProducto;
    private Integer existenciaDelProducto;
    private String origen;
    

    public Producto(String codigoID, String descripcionDelProducto,
    Float precioDelProducto, Integer existenciaDelProducto, String origen){
        this.codigoID = codigoID;
        this.descripcionDelProducto = descripcionDelProducto;
        this.precioDelProducto = precioDelProducto;
        this.existenciaDelProducto = existenciaDelProducto;
        this.origen = origen;
    }    
   
    public String getCodigoDeIdentificacion(){
        return codigoID;
    }

    public String getDescripcionDelProducto(){
        return descripcionDelProducto;
    }

    public Float getPrecioDelProducto(){
        return precioDelProducto;
    }

    public Integer getExistenciaDelProducto(){
        return existenciaDelProducto;
    }

    public String getOrigenDelProducto(){
        return origen;
    }
}
