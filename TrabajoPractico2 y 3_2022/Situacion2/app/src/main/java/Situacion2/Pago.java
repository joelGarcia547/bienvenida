package Situacion2;

public interface Pago {
    public String efectivo(Float monto);
    public String cheque(Float monto);
    public String tarjetaDeCredito(Float monto);
}
