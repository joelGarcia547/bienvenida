package Situacion2;

public class LineaDePedido {
    private Producto producto;
    static Float impuestoDeRetencion = (2f/100);
    static Float impuestoDeAduana = (1f/100);
    static Float impuestoDeTransporte = (0.05f/100);
    private Integer cantidadDelProducto;
    
    public LineaDePedido(Producto producto, Integer cantidadDelProducto){
        this.producto = producto;
        this.cantidadDelProducto = cantidadDelProducto;
    }

    public Producto getProducto(){
        return producto;
    }

    public Integer getCantidadDelProducto(){
        return cantidadDelProducto;
    }

    public Float getPrecioSubtotal(){
        Float subTotal = 0.0f;
        subTotal = ((subTotal + producto.getPrecioDelProducto()) * cantidadDelProducto);
        //precio individual de cada producto * cantidad del mismo = subtotal (sin envio, ni impuesto)
        if(producto.getOrigenDelProducto().equalsIgnoreCase("nacional")){
            subTotal = (subTotal - (subTotal * impuestoDeRetencion));
        } 

        if(producto.getOrigenDelProducto().equalsIgnoreCase("importado")){
            subTotal = (subTotal + (subTotal * impuestoDeAduana));
            subTotal = (subTotal + (subTotal * impuestoDeTransporte));
        }
        
        return subTotal;
    }

}
