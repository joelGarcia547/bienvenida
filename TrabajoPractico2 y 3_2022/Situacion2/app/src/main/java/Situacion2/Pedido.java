package Situacion2;

import java.util.ArrayList;
import java.util.List;

import Exceptions.LineaDePedidoInexistenteEnRegistroException;

public class Pedido implements Pago{
    private List<LineaDePedido> listaDeProducto; //un pedido puede tener 1..* productos
    private String estadoDelPedido; //pendiente, pagado, procesando, enviado y entregado 
    private String formaDepago; //efectivo mediante tarjeta de crédito y cheque

    public Pedido(String estadoDelPedido, String formaDepago){
        this.estadoDelPedido = estadoDelPedido;
        this.formaDepago = formaDepago;
        listaDeProducto = new ArrayList<>();
    }

    public String getEstadoDelPedido(){
        return estadoDelPedido;
    }

    public List<LineaDePedido> getListaDeProducto(){
        return listaDeProducto;
    }

    public String getFormaDePago(){
        return formaDepago;
    }

    //metodos necesarios para listar "Local.listarPedido()"
    public String retornarDescripcionDelProducto(Integer _index){
        return listaDeProducto.get(_index).getProducto().getDescripcionDelProducto();
    }

    public String retornarOrigenDelProducto(Integer _index){
        return listaDeProducto.get(_index).getProducto().getOrigenDelProducto();
    }

    public String retornarCodigoDelProducto(Integer _index){
        return listaDeProducto.get(_index).getProducto().getCodigoDeIdentificacion();
    }

    //crear, buscar, eliminar lista de producto
    public void crearLineaDePedido(Producto producto, Integer cantidad){
        listaDeProducto.add(new LineaDePedido(producto, cantidad));
    }

    public LineaDePedido buscarLineaDePedido(String _codigoDeProductoABuscar){
        LineaDePedido _lineaDePedidoARetornar = null;
        for(LineaDePedido _linea : listaDeProducto){
            if(_linea.getProducto().getCodigoDeIdentificacion().equals(_codigoDeProductoABuscar)){
                _lineaDePedidoARetornar = _linea;
            }
        }
        if(_lineaDePedidoARetornar == null){
            throw new LineaDePedidoInexistenteEnRegistroException();
        }
        return _lineaDePedidoARetornar;
    }

    public void eliminarLineaDePedido(String _codigoDeProductoAEliminar){
        LineaDePedido _lineaAEliminar = buscarLineaDePedido(_codigoDeProductoAEliminar);
        listaDeProducto.remove(_lineaAEliminar);
    }
    
    
    public Float precioTotalDelPedido(){
        Float precioTotal = 0.0f;  
        for(LineaDePedido _lineaAComparar : listaDeProducto){
            precioTotal = (precioTotal + _lineaAComparar.getPrecioSubtotal());
        }

        return precioTotal;
    }

    public String efectivo(Float monto) {
        return (">--- Se pago en efectivo ---<\n+ Total: "+monto); 
    }

    public String cheque(Float monto) {
        return (">--- Se pago con cheque ---<\n+ Total: "+monto);
    }

    public String tarjetaDeCredito(Float monto) {
        return(">--- Se pago con tarjeta ---<\n+ Total: "+monto);

    }
   
}
