package Situacion2;

import java.util.ArrayList;
import java.util.List;

import Exceptions.FormaDePagoNoValidaEnSistemaException;
import Exceptions.PedidoInexistenteEnListaException;

public class Local{
    private String contacto;
    private List<Pedido> listaDePedido;

    public Local( String contacto){
        this.contacto = contacto;
        listaDePedido = new ArrayList<>();
    }

    public String getContacto(){
        return contacto;
    }

    public List<Pedido> getListaDePedidos(){
        return listaDePedido;
    }

    //agregar, buscar, listar y eliminar pedido
    public void agregarPedidoALista(Pedido pedido){
        listaDePedido.add(pedido);
    }

    public Pedido buscarPedidoEnLista(Pedido _pedidoABuscar){
        Pedido _pedidoEncontrado = null;
        for(Pedido _pedido : listaDePedido){
            if(_pedido.equals(_pedidoABuscar)){
                _pedidoEncontrado = _pedido;
            }
        }
        if(_pedidoABuscar == null){
            throw new PedidoInexistenteEnListaException();
        }
        return _pedidoEncontrado;
    }

    public void eliminarPedidoDeLaLista(Pedido pedidoABorrar){
        Pedido _pedidoAEliminar = buscarPedidoEnLista(pedidoABorrar);
        listaDePedido.remove(_pedidoAEliminar);
    }

    public void listarPedido(){
        System.out.println("................................................");
        System.out.println("------ Nomina de Pedidos en existencia:  ------\n");
        for(Pedido _listar : listaDePedido){                   //se pierde la referencia desde .get(index).getProducto().getDescripcionDelProducto());        
            Integer _index = 0;
            System.out.println("+ Identificacion del producto (ID): "+_listar.retornarCodigoDelProducto(_index));
            System.out.println("+ Descripcion: "+_listar.retornarDescripcionDelProducto(_index));
            System.out.println("+ Origen: "+_listar.retornarOrigenDelProducto(_index));
            System.out.println(""+listarFormaDePago(_listar)+"\n");
            _index++;
        }
        System.out.println("................................................");      
    }

    public String listarFormaDePago(Pedido _formaDePagoDelPedido){
        Float _monto = _formaDePagoDelPedido.precioTotalDelPedido();
        String _retornarFormaDePago = null;
        
        if(_formaDePagoDelPedido.getFormaDePago().equalsIgnoreCase("efectivo")){
            _retornarFormaDePago = _formaDePagoDelPedido.efectivo(_monto);
        } 

        if(_formaDePagoDelPedido.getFormaDePago().equalsIgnoreCase("cheque")){
            _retornarFormaDePago = _formaDePagoDelPedido.cheque(_monto);
        } 

        if(_formaDePagoDelPedido.getFormaDePago().equalsIgnoreCase("tarjeta de credito")){
            _retornarFormaDePago = _formaDePagoDelPedido.tarjetaDeCredito(_monto);
        }
            
        if(_retornarFormaDePago == null){
            throw new FormaDePagoNoValidaEnSistemaException();
        }

        return _retornarFormaDePago;
    }

    

}
