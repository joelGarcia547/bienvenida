package Situacion2;

public class Cliente {
    private String nombre;
    private String apellido;
    private String documento;
    private String domicilio;

    //constructor
    public Cliente(String nombre, String apellido, String documento, String domicilio){
        this.nombre = nombre;
        this.apellido = apellido;
        this.documento = documento;
        this.domicilio = domicilio;
    }
    //metodos accesores
    public String getNombre(){
        return nombre;
    }

    public String getApellido(){
        return apellido;
    }

    public String getDocumento(){
        return documento;
    }

    public String getDomicilio(){
        return domicilio;
    }

}
