package Situacion2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LocalTest {
    @Test
    public void verificarAgregadoDePedidoALista(){
        Pedido _pedido0 = new Pedido("Pagado", "efectivo");
        Pedido _pedido1 = new Pedido("Procesando", "Tarjeta DE CREDITO");
        Pedido _pedido2 = new Pedido("Enviado", "CHEQUE");
        Local _tiendaElectronica = new Local("@ElectroFriend_");

        _tiendaElectronica.agregarPedidoALista(_pedido0);
        _tiendaElectronica.agregarPedidoALista(_pedido1);
        _tiendaElectronica.agregarPedidoALista(_pedido2);

        assertEquals(3, _tiendaElectronica.getListaDePedidos().size());
    }

    @Test
    public void verificarBusquedaDePedidoEnLista(){
        Pedido _pedidoA = new Pedido("Entregado", "efectivo");
        Pedido _pedidoB = new Pedido("pagado", "efectivo");
        Local _tiendaDeRopa = new Local("@IndumentariaSarmiento");

        _tiendaDeRopa.agregarPedidoALista(_pedidoA);
        _tiendaDeRopa.agregarPedidoALista(_pedidoB);
        Pedido _pedidoEncontrado = _tiendaDeRopa.buscarPedidoEnLista(_pedidoB);

        assertEquals(_pedidoB, _pedidoEncontrado);
    }

    @Test
    public void verificarEliminadoDePedidoEnLista(){
        Pedido _pedido00 = new Pedido("pendiente", "cheque");
        Local _local = new Local("@GriferiaPetter's");

        _local.agregarPedidoALista(_pedido00);
        _local.eliminarPedidoDeLaLista(_pedido00);

        assertEquals(0, _local.getListaDePedidos().size());
    }

}
