package Situacion2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LineaDePedidoTest {
    @Test 
    public void verificarPrecioSubTotalConImpuesto(){
        Producto productoA = new Producto("93F62", " camisa", 3600f,120, "importado");
        LineaDePedido lineaDePedido = new LineaDePedido(productoA, 5);
        

        assertEquals(18189.4f, lineaDePedido.getPrecioSubtotal(), 0.5f);
    }
}
