package Situacion2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PedidoTest {
    @Test
    public void verificarCreacionDeLineaDePedido(){
        Producto productoA = new Producto("5AF14","Flor de plastico", 380f, 260, "importado");
        Producto productoB = new Producto("B52F3", "Vaso de vidrio", 130f,380, "nacional");
        Pedido pedido = new Pedido("Pendiente", "tarjeta De credito");

        pedido.crearLineaDePedido(productoA, 8); //posicion [0]
        pedido.crearLineaDePedido(productoB, 34); //posicion [1]

        assertEquals(2, pedido.getListaDeProducto().size());
    }

    @Test
    public void verificarBusquedaDeLineaDePedidoEnLista(){
        Producto _producto0 = new Producto("05BC5", "Control de televisor smart", 1600f, 150, "nacional");
        Producto _producto1 = new Producto("64DA9", "Licuadora", 9500f, 80, "nacional");
        Pedido _pedido = new Pedido("Entregado", "cheque");
        
        _pedido.crearLineaDePedido(_producto0, 2);
        _pedido.crearLineaDePedido(_producto1, 1);

        assertEquals(_pedido.getListaDeProducto().get(1), _pedido.buscarLineaDePedido(_producto1.getCodigoDeIdentificacion()));
    }

    @Test
    public void verificarEliminadoDeLineaDePedidoEnLista(){
        Producto _productoA = new Producto("67D2A", "mouse de pc para oficina", 550f, 360, "nacional");
        Pedido _pedido = new Pedido("Entregado", "tarjeta de credito");
   
        _pedido.crearLineaDePedido(_productoA, 120);
        _pedido.eliminarLineaDePedido(_productoA.getCodigoDeIdentificacion());

        assertEquals(0, _pedido.getListaDeProducto().size());
    }

    @Test
    public void verificarPrecioTotalDelPedido(){
        Producto producto1 = new Producto("1C73A", "Remera", 1400f,170,"nacional");
        Producto producto2 = new Producto("72D1B", "Corbata negra", 1250f, 160,"importado");
        Producto producto3 = new Producto("3E70C", "Gorro de cumpleaños", 50f,450,"nacional");
        Pedido pedido2 = new Pedido("Pagado","efectivo");

        pedido2.crearLineaDePedido(producto1, 3);
        pedido2.crearLineaDePedido(producto2, 2);
        pedido2.crearLineaDePedido(producto3, 6);

        assertEquals(6936.4f, pedido2.precioTotalDelPedido(), 0.5f);
    }
}
