package Situacion1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ComisionadoTest {
    @Test
    public void verificarComisionPorVentaExitosa(){
        Comisionado _comisionado = new Comisionado(55000f, "limpieza", 350f);
        Empleado _empleado0 = new Empleado("dario", "menendez", "43890123", "@Menendez_", _comisionado, 36000f, true);
        Venta _venta = new Venta(_empleado0, "exitosa", 4700f);

        _comisionado.comisionPorVentaExitosa(_venta);

        assertEquals(5875.4f, _comisionado.getComision(), 0.5f);
    }
}
