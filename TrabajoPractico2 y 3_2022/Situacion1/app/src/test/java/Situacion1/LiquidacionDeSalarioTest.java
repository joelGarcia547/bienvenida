package Situacion1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

public class LiquidacionDeSalarioTest {
    @Test
    public void verificarAgregadoDePagoAlRegistro(){
        EmpresaContratante emp = new EmpresaContratante();
        LiquidacionDeSalario liquidacion = new LiquidacionDeSalario(LocalDate.now(), emp);
        
        liquidacion.agregarPagoAlRegistro(LocalDate.of(2022, 8, 21));
        liquidacion.agregarPagoAlRegistro(LocalDate.of(2022, 7, 20));

        assertEquals(2,liquidacion.getRegistroDePago().size());
    }

    @Test
    public void verificarBusquedaDePagoEnRegistro(){
        EmpresaContratante _emp = new EmpresaContratante();
        LiquidacionDeSalario _fechaDePago = new LiquidacionDeSalario(LocalDate.now(), _emp);
        LocalDate _fechaEncontrada = LocalDate.of(2021, 11, 23);

        _fechaDePago.agregarPagoAlRegistro(LocalDate.of(2022, 8, 17));
        _fechaDePago.agregarPagoAlRegistro(LocalDate.of(2021, 11, 23));

        assertEquals(_fechaEncontrada, _fechaDePago.buscarPagoEnRegisto(LocalDate.of(2021, 11, 23)));
    }

    @Test
    public void verificarEliminacionDeRegistroDePago(){
        EmpresaContratante emp1 = new EmpresaContratante();
        LiquidacionDeSalario liquidacion2 = new LiquidacionDeSalario(LocalDate.now(), emp1);

        liquidacion2.agregarPagoAlRegistro(LocalDate.of(2022, 3, 15)); 
        liquidacion2.removerPagoDelRegistro(LocalDate.of(2022, 3, 15)); //posicion [0], se elimina

        assertEquals(0, liquidacion2.getRegistroDePago().size()); //posicion [1] pasa a ser [0]
    }

    @Test
    public void verificarPagoDeSueldoAEmpleado(){
        EmpresaContratante empresa = new EmpresaContratante();
        CategoriaDeEmpleado categoria = new CategoriaDeEmpleado(30000f, "basico");
        Empleado emp1 = new Empleado("Roberto", "Martinez", "43576423", "(alias) _martinR", categoria, 55000.6f,true);
        LiquidacionDeSalario liqui = new LiquidacionDeSalario(LocalDate.now(), empresa);

        empresa.agregarEmpleadoANomina(emp1);
        Empleado _empleadoPago = liqui.pagarSueldoAEmpleado("43576423");

        assertFalse(_empleadoPago.getSueldoNoAsignado());
    }

    @Test
    public void verificarLiquidacionDeEmpresaContratante(){
        EmpresaContratante _empresa = new EmpresaContratante();
        LiquidacionDeSalario _liquidacion = new LiquidacionDeSalario(LocalDate.of(2022, 10, 15), _empresa);
      
        _liquidacion.liquidacionDeEmpresaContratante(7600f, LocalDate.of(2022, 9, 14));
        _liquidacion.liquidacionDeEmpresaContratante(5600f, LocalDate.now());

        assertEquals(13200.04f, _empresa.getPagoTotal(), 0.5f);
    }

    @Test
    public void verificarCreacionDeBonificacionesParaEmpleados(){
        EmpresaContratante _emp = new EmpresaContratante();
        LiquidacionDeSalario _liquidacion = new LiquidacionDeSalario(LocalDate.now(), _emp);
        CategoriaDeEmpleado _categoria1 = new CategoriaDeEmpleado(60400f, "comisionado");
        Empleado _empleado0 = new Empleado("manuel", "Navarro", "43657987", "@alias.tecno.unca", _categoria1, 70000f, false);
    
        _emp.agregarEmpleadoANomina(_empleado0);
        _liquidacion.crearBonificacion(_empleado0);

        assertEquals(96640.04f, _empleado0.getCategoria().getSueldoBase(), 0.5f);
    }

    @Test
    public void verificarLaAplicacionDeRetencionDeImpuesto(){
        EmpresaContratante _empresa = new EmpresaContratante();
        LiquidacionDeSalario _liqui = new LiquidacionDeSalario(LocalDate.now(), _empresa);
        CategoriaDeEmpleado _categoriaA = new CategoriaDeEmpleado(45000f, "basico");
        Empleado _empleado = new Empleado("Menphis", "parris", "43465645", "@Memphis00.bnv", _categoriaA, 38000f, false);

        _empresa.agregarEmpleadoANomina(_empleado);
        _liqui.aplicarRetencionDeImpuesto(_empleado);
        
        assertEquals(44910.04f , _empleado.getCategoria().getSueldoBase(), 0.5f);
    }
}
