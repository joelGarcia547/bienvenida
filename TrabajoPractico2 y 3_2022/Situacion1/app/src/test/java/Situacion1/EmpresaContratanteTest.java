package Situacion1;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

public class EmpresaContratanteTest {
    @Test
    public void verificarAgregadoDePagoUnicoANomina(){
        EmpresaContratante _empresaC = new EmpresaContratante();
        _empresaC.agregarPagoUnicoALista(LocalDate.now(), 6700f);
        _empresaC.agregarPagoUnicoALista(LocalDate.of(2022, 10, 16), 5788f);

        assertEquals(2, _empresaC.getNominaDePagos().size());
    }

    @Test
    public void verificarBusquedaDeNominaDePago(){
        EmpresaContratante _empresaA = new EmpresaContratante();
        _empresaA.agregarPagoUnicoALista(LocalDate.now(), 3478f);
        _empresaA.agregarPagoUnicoALista(LocalDate.now(), 8674f);

        assertEquals(_empresaA.getNominaDePagos().get(1), _empresaA.buscarNominaDePago(LocalDate.now()));
    }

    @Test
    public void verificarEliminacionDeNominaDePago(){
        EmpresaContratante _empresaB = new EmpresaContratante();
        _empresaB.agregarPagoUnicoALista(LocalDate.now(), 7656f);
        _empresaB.eliminarNominaDePago(LocalDate.now());

        assertEquals(0, _empresaB.getNominaDePagos().size());
    }

    @Test
    public void verificarTotalAPagarEnNominaDePago(){
        EmpresaContratante _empresaD = new EmpresaContratante();
        _empresaD.agregarPagoUnicoALista(LocalDate.of(2022, 10, 16), 5666f);
        _empresaD.agregarPagoUnicoALista(LocalDate.now(), 6788f);

        assertEquals(12454.04f, _empresaD.totalAPagar(), 0.5f);

    }
}
