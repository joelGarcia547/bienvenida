package Situacion1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EmpresaTest {
    @Test
    public void verificarAgregadoANominaDeEmpleado(){
        CategoriaDeEmpleado categoria1 = new CategoriaDeEmpleado(88000f, "Mantenimiento");
        CategoriaDeEmpleado categoria2 = new CategoriaDeEmpleado(75000f, "Seguridad");
        Empleado empleado1 = new Empleado("Carlos", "Martinez", "43783123", "(alias) carlosMartinez_", categoria1,50000f, true);
        Empleado empleado2 = new Empleado("Roberto", "Perez", "43456243", "(alias) robertP_", categoria2,50000f,false);
        Empresa empresa = new EmpresaContratante();

        empresa.agregarEmpleadoANomina(empleado1);
        empresa.agregarEmpleadoANomina(empleado2);

        assertEquals(2, empresa.getNominaDeEmpleados().size());
    }

    @Test
    public void verificarBusquedaDeEmpleadoEnNomina(){
        EmpresaContratante empresa = new EmpresaContratante();
        CategoriaDeEmpleado categoria0 = new CategoriaDeEmpleado(45000f, "Electricidad");
        Empleado _emp1 = new Empleado("Carlos", "cruz", "43453123", "@cruz.Carlos", categoria0, 35000f, true);
        
        empresa.agregarEmpleadoANomina(_emp1);

        assertEquals(_emp1, empresa.buscarEmpleado(_emp1.getDocumento()));
    }

    @Test
    public void verificarEliminacionDeEmpleadoDeLaNomina(){
        Comisionado categoriaA = new Comisionado(50000f, "Comisionado", 500f);
        Empleado empleadoA = new Empleado("Maria", "Paredes", "43522343", "100164735548212412415", categoriaA,65000f,false);
        Empresa empresa2 = new EmpresaContratante();

        empresa2.agregarEmpleadoANomina(empleadoA); 
        empresa2.removerEmpleadoDeNomina(empleadoA); 
    
        assertEquals(0, empresa2.getNominaDeEmpleados().size()); 
    }

    @Test
    public void verificarAgregadoDeVentaALista(){
        EmpresaContratante _empresa = new EmpresaContratante();
        Comisionado _com = new Comisionado(42000f, "mantenimiento", 760f);
        Empleado _emp2 = new Empleado("nahuel", "tapia", "43765231", "@Tapia.Nah", _com, 28000f, true);

        _empresa.agregarUnaVentaALista(_emp2, "Exitosa", 5000f);

        assertEquals(1, _empresa.getListaDeVenta().size());
    }

    @Test
    public void verificarBusquedaDeVentaEnLista(){
        EmpresaContratante _empresa = new EmpresaContratante();
        Comisionado _comisionado = new Comisionado(60000f, "mantenimiento", 500f);
        Empleado _emp3 = new Empleado("fernando", "vega", "43576123", "@Vega.F", _comisionado, 48000f, false);

        _empresa.agregarUnaVentaALista(_emp3, "fallida", 5677f);

        assertEquals(_empresa.getListaDeVenta().get(0), _empresa.buscarVenta("43576123"));
    }

    @Test
    public void verificarEliminacionDeVentaEnLista(){
        EmpresaContratante _empresa = new EmpresaContratante();
        Comisionado _comisionado = new Comisionado(40000f, "limpieza", 860f);
        Empleado _emp2 = new Empleado("Manuel", "collins", "43965231", "@collins_", _comisionado, 38000f, true);

        _empresa.agregarUnaVentaALista(_emp2, "En proceso", 4000f);
        _empresa.removerVentaDeNomina(_emp2.getDocumento());

        assertEquals(0, _empresa.getListaDeVenta().size());

    }
}
