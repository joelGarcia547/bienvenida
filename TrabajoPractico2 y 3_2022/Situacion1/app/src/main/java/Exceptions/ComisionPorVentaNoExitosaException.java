package Exceptions;

public class ComisionPorVentaNoExitosaException extends RuntimeException{
    public ComisionPorVentaNoExitosaException(){
        super("No hay comision ya que la venta no fue exitosa");
    }
}
