package Exceptions;

public class FechaDePagoInexistenteEnRegistroException extends RuntimeException{
    public FechaDePagoInexistenteEnRegistroException(){
        super("La fecha de pago no se encuentra estipulada en el registro");
    }
}
