package Exceptions;

public class VentaInexistenteEnNominaException extends RuntimeException{
    public VentaInexistenteEnNominaException(){
        super("La venta que busco no existe en la nomina");
    }
}
