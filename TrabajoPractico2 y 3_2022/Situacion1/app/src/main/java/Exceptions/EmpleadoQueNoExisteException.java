package Exceptions;

public class EmpleadoQueNoExisteException extends RuntimeException{
    public EmpleadoQueNoExisteException(){
        super("El empleado que se busco en nomina no existe");
    }
}
