package Exceptions;

public class PagoInexistenteEnNominaException extends RuntimeException{
    public PagoInexistenteEnNominaException(){
        super("El pago no se encuentra en el registro de pagos efectuados");
    }
}
