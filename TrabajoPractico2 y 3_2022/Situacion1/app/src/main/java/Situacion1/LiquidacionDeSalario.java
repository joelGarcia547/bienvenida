package Situacion1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import Exceptions.EmpleadoQueNoExisteException;
import Exceptions.FechaDePagoInexistenteEnRegistroException;

public class LiquidacionDeSalario implements BonificacionSemanal, RetencionDeImpuesto{
    private LocalDate fechaDeLiquidacion;
    private List<LocalDate> registroDePagos;
    private static Float sueldoAPagar = 60000f; 
    private EmpresaContratante empresa;
    
    public LiquidacionDeSalario(LocalDate fechaDeLiquidacion, EmpresaContratante empresa){
        this.fechaDeLiquidacion = fechaDeLiquidacion;
        this.empresa = empresa;
        registroDePagos = new ArrayList<>();
    }

    public LocalDate getFechaDeLiquidacion(){
        return fechaDeLiquidacion;
    }

    public List<LocalDate> getRegistroDePago(){
        return registroDePagos;
    }
    
    public Empresa getEmpresa(){
        return empresa;
    }

    //agregar, buscar, remover y listar registro de pagos (fechas de pago)
    public void agregarPagoAlRegistro(LocalDate diaDePago){
        registroDePagos.add(diaDePago);
    }

    public LocalDate buscarPagoEnRegisto(LocalDate _fechaABuscar){
        LocalDate fechaADevolver = null;
        for(LocalDate _registro : registroDePagos){
            if(_registro.equals(_fechaABuscar)){
                fechaADevolver = _registro;
            }
        }
        if(fechaADevolver == null){
            throw new FechaDePagoInexistenteEnRegistroException();
        }
        return fechaADevolver;
    }

    public void removerPagoDelRegistro(LocalDate diaDePagoARemover){
        LocalDate fechaDePagoARemover = buscarPagoEnRegisto(diaDePagoARemover);
        registroDePagos.remove(fechaDePagoARemover);
    }

    public void listarRegistroDePagos(){
        System.out.println("------------------------------------------");
        for (LocalDate _registro : registroDePagos){
            System.out.println("DD/MM/YYYY : " + _registro.getDayOfMonth() + "/" + _registro.getMonthValue() + "/" + _registro.getYear() + "\n");
        }
        System.out.println("------------------------------------------");
    }

    //pagar sueldo a empleados
    public Empleado pagarSueldoAEmpleado(String _dniDeEmpleadoALiquidar){
        Empleado empleadoALiquidar = empresa.buscarEmpleado(_dniDeEmpleadoALiquidar);
        Boolean _condicionDePago = empleadoALiquidar.getSueldoNoAsignado(); //true: no se pago

        if((empleadoALiquidar != null) && (_condicionDePago)){ //si != null quiere decir que el empleado esta en la nomina
            if(sueldoAPagar > empleadoALiquidar.getCategoria().getSueldoBase()){
                sueldoAPagar = (sueldoAPagar + (sueldoAPagar - empleadoALiquidar.getCategoria().getSueldoBase()));      
            } 
            sueldoAPagar = (empleadoALiquidar.getCategoria().getSueldoBase() + (empleadoALiquidar.getCategoria().getSueldoBase() - sueldoAPagar));    
    
            empleadoALiquidar.getCategoria().setSueldoBase(sueldoAPagar);
            empleadoALiquidar.setSueldoNoAsignado(false);
        } else {
            throw new EmpleadoQueNoExisteException();
        }

        return empleadoALiquidar;  
    }  
            
    //pago a empresa contratante
    public void liquidacionDeEmpresaContratante(Float _pagoDeEmpresa, LocalDate _fechaDelPago){
        empresa.agregarPagoUnicoALista(_fechaDelPago, _pagoDeEmpresa);
        Float pagoALiquidar =  empresa.totalAPagar();
        empresa.setPagoTotal(pagoALiquidar);
    }

    //Bonificaciones porcentuales semanales
    public void crearBonificacion(Empleado _empleado) {
        Empleado _empleadoABonificar = empresa.buscarEmpleado(_empleado.getDocumento());
        Float bonificacion = 0f;

        if(_empleadoABonificar.getSueldoNoAsignado()){ //true: todavia no se pago
            _empleadoABonificar = pagarSueldoAEmpleado(_empleado.getDocumento());
        } 
        
        if(_empleadoABonificar != null){
            if(_empleadoABonificar.getCategoria() instanceof Comisionado){
                Comisionado _CategoriaComisionado = (Comisionado) _empleadoABonificar.getCategoria();
                bonificacion = (_CategoriaComisionado.getSueldoBase() + (_CategoriaComisionado.getSueldoBase() * multiplicadorParaEmpleadoComisionado));
            } 
        } else {
            throw new EmpleadoQueNoExisteException();
        }
            
        bonificacion = (_empleadoABonificar.getCategoria().getSueldoBase() + (_empleadoABonificar.getCategoria().getSueldoBase() * multiplicadorParaEmpleadoBasico));

        _empleadoABonificar.getCategoria().setSueldoBase(bonificacion); 
    }

    //retencion de impuesto
    public void aplicarRetencionDeImpuesto(Empleado _empleado) {
        Empleado _empleadoAAplicarRetencion = empresa.buscarEmpleado(_empleado.getDocumento());
        Float _sueldoConImpuesto = 0f;

        if(_empleadoAAplicarRetencion.getSueldoNoAsignado()){ //true: todavia no se pago
            _empleadoAAplicarRetencion = pagarSueldoAEmpleado(_empleado.getDocumento()); //todavia no se le pago
        }
       
        if(_empleadoAAplicarRetencion != null){
            if(_empleadoAAplicarRetencion.getCategoria().getSueldoBase() > _empleadoAAplicarRetencion.getAsignacionLibreDeImpuesto()){
                _sueldoConImpuesto = (_empleadoAAplicarRetencion.getCategoria().getSueldoBase() - (_empleadoAAplicarRetencion.getCategoria().getSueldoBase() * tasaImpositivaGlobal));
                _empleadoAAplicarRetencion.getCategoria().setSueldoBase(_sueldoConImpuesto);
            } 
        } else {
            throw new EmpleadoQueNoExisteException();
        }
               

    }

}


