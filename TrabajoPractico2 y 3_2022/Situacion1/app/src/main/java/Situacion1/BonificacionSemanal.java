package Situacion1;

public interface BonificacionSemanal {
    static Float multiplicadorParaEmpleadoComisionado = 1.5f;
    static Float multiplicadorParaEmpleadoBasico = 0.6f;
    public abstract void crearBonificacion(Empleado _empleadoABonificar);
}
