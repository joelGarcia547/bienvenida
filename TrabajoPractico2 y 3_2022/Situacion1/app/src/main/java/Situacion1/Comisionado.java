package Situacion1;

import Exceptions.ComisionPorVentaNoExitosaException;

public class Comisionado extends CategoriaDeEmpleado{
    private Float comision;
    static Float porcentajeDeComision = 1.25f;

    public Comisionado(Float _sueldoBase, String _nombreDeLaCategoria, Float comision){
        super(_sueldoBase, _nombreDeLaCategoria);
        this.comision = comision;
    }

    public void setComision(Float _comision){
        this.comision = _comision;
    }
    public Float getComision(){
        return comision;
    }
    
    public void comisionPorVentaExitosa(Venta _venta){
        if(_venta.getEstadoDeLaVenta().equalsIgnoreCase("Exitosa")){
           this.comision = (_venta.getPrecioDeVenta() * porcentajeDeComision);
        } else {
            throw new ComisionPorVentaNoExitosaException();
        }
    }

    
}
