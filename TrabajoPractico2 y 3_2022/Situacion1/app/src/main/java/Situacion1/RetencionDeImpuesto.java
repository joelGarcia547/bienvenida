package Situacion1;

public interface RetencionDeImpuesto {
    static Float tasaImpositivaGlobal = (0.2f/100f); //20% de $100 = ((100 * 20)/100)
    public abstract void aplicarRetencionDeImpuesto(Empleado _empleado);
}
