package Situacion1;

import java.time.LocalDate;

public class NominaDePago {
    private LocalDate fechaDePago;
    private Float pagoUnico;

    public NominaDePago(LocalDate fechaDePago, Float pagoUnico){
        this.fechaDePago = fechaDePago;
        this.pagoUnico = pagoUnico;
    }

    public LocalDate getFechaDePago(){
        return fechaDePago;
    }
    public Float getPagoUnico(){
        return pagoUnico;
    }
}
