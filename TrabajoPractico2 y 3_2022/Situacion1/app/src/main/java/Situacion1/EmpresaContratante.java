package Situacion1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import Exceptions.PagoInexistenteEnNominaException;

public class EmpresaContratante extends Empresa{
    private List<NominaDePago> listaDePagosUnicos;
    private Float pagoTotal;

    public EmpresaContratante(){
        super();
        listaDePagosUnicos = new ArrayList<>();
    }

    public List<NominaDePago> getNominaDePagos(){
        return listaDePagosUnicos;
    }

    public void setPagoTotal(Float _pagoAcumuladoALiquidar){
        this.pagoTotal = _pagoAcumuladoALiquidar;
    }
    public Float getPagoTotal(){
        return pagoTotal;
    }

    //agregar, buscar, remover y listar pagos a empresa contratante
    public void agregarPagoUnicoALista(LocalDate _FechaDelPago,Float _precioDePagoUnico){
        listaDePagosUnicos.add(new NominaDePago(_FechaDelPago, _precioDePagoUnico));
    }

    public NominaDePago buscarNominaDePago(LocalDate _FechaCuandoSeRealizoElPago){
        NominaDePago nominaEncontrada = null;
        for(NominaDePago _nomina : listaDePagosUnicos){
            if(_nomina.getFechaDePago().equals(_FechaCuandoSeRealizoElPago)){
                nominaEncontrada = _nomina;
            }
        }
        if(nominaEncontrada == null){
            throw new PagoInexistenteEnNominaException();
        }
        return nominaEncontrada;
    }

    public void eliminarNominaDePago(LocalDate _fechaABuscar){
        NominaDePago _nominaABorrar = buscarNominaDePago(_fechaABuscar);
        listaDePagosUnicos.remove(_nominaABorrar);
    }

    public void listarPagosUnicos(){
        System.out.println("Pagos Unicos: ");
        for(NominaDePago _pago : listaDePagosUnicos){
            System.out.println("> "+_pago.getPagoUnico()+"  |  Fecha: "+_pago.getFechaDePago());
        }
    }

    public Float totalAPagar(){
        Float _pagoAcumulado = 0f;
        for(NominaDePago _nomina : listaDePagosUnicos){
            _pagoAcumulado = (_pagoAcumulado + _nomina.getPagoUnico());
        }

        return _pagoAcumulado;
    }
 
    
}
