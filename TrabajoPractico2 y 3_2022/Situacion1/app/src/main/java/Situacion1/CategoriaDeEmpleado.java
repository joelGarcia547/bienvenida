package Situacion1;

public class CategoriaDeEmpleado{
    private Float sueldoBase;
    private String nombreDeLaCategoria;
   
    //constructor
    public CategoriaDeEmpleado(Float sueldoBase, String nombreDeLaCategoria){
        this.sueldoBase = sueldoBase;
        this.nombreDeLaCategoria = nombreDeLaCategoria;
    }

    //metodos accesores
    public void setSueldoBase(Float sueldoBase){
        this.sueldoBase = sueldoBase;
    }
    public Float getSueldoBase(){
        return sueldoBase;
    }

    public String getNombreDeLaCategoria(){
        return nombreDeLaCategoria;
    }
    
}
