package Situacion1;

public class Venta {
    private Empleado empleado; //empleado que realizo la venta
    private Float precioDeVenta;
    private String estadoDeLaVenta; //exitosa o fallida

    public Venta(Empleado empleado, String estadoDeLaVenta, Float precioDeVenta){
        this.empleado = empleado;
        this.estadoDeLaVenta = estadoDeLaVenta;
        this.precioDeVenta = precioDeVenta;
    }

    public Empleado getEmpleado(){
        return empleado;
    }

    public Float getPrecioDeVenta(){
        return precioDeVenta;
    }

    public String getEstadoDeLaVenta(){
        return estadoDeLaVenta;
    }
}
