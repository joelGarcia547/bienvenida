package Situacion1;

public class Empleado { //encapsulamiento y metodos accesores
    private String nombre;
    private String apellido;
    private String documento;
    private String claveBancariaUniforme;
    private CategoriaDeEmpleado categoria;
    private Float asignacionLibreDeImpuesto;
    private Boolean sueldoNoAsignado; //true: no se asigno, false: ya se asigno

    //constructor
    public Empleado(String nombre, String apellido, String documento,
    String claveBancariaUniforme, CategoriaDeEmpleado categoria, Float asignacionLibreDeImpuesto,
    Boolean sueldoNoAsignado){
        this.nombre = nombre;
        this.apellido = apellido;
        this.documento = documento;
        this.claveBancariaUniforme = claveBancariaUniforme;
        this.categoria = categoria;
        this.asignacionLibreDeImpuesto = asignacionLibreDeImpuesto;
        this.sueldoNoAsignado = sueldoNoAsignado;
    }
    //metodos accesores
    public String getNombre(){
        return nombre;
    }

    public String getApellido(){
        return apellido;
    }

    public String getDocumento(){
        return documento;
    }

    public String getClaveBancariaUniforme(){
        return claveBancariaUniforme;
    }

    public CategoriaDeEmpleado getCategoria(){
        return categoria;
    }

    public Float getAsignacionLibreDeImpuesto(){
        return asignacionLibreDeImpuesto;
    }

    public void setSueldoNoAsignado(Boolean _condicion){
        this.sueldoNoAsignado = _condicion;
    }
    public Boolean getSueldoNoAsignado(){
        return sueldoNoAsignado;
    }
    
}
