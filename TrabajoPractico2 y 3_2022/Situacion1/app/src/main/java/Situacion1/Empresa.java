package Situacion1;

import java.util.ArrayList;
import java.util.List;

import Exceptions.EmpleadoQueNoExisteException;
import Exceptions.VentaInexistenteEnNominaException;

public abstract class Empresa {
    private List<Empleado> nominaDeEmpleado;   //lista de empleados de la empresa
    private List<Venta> listaDeVenta;

    public Empresa(){
        nominaDeEmpleado = new ArrayList<>();
        listaDeVenta = new ArrayList<>();
    }
    //metodos accesores
    public List<Empleado> getNominaDeEmpleados(){
        return nominaDeEmpleado;    //retorna la nomina de empleados de la empresa
    }

    public List<Venta> getListaDeVenta(){
        return listaDeVenta;
    }

    //agregar, buscar, eliminar y listar empleados
    public void agregarEmpleadoANomina(Empleado emp1){ 
        nominaDeEmpleado.add(emp1); 
    }

    public Empleado buscarEmpleado(String _documentoABuscar){
        Empleado empleadoADevolver = null;
        for (Empleado _empleado : nominaDeEmpleado){
            if(_empleado.getDocumento().equals(_documentoABuscar)){
                empleadoADevolver = _empleado;
            }
        }
        if(empleadoADevolver == null){
            throw new EmpleadoQueNoExisteException();
        }
        return empleadoADevolver;
    }

    public void removerEmpleadoDeNomina(Empleado emp2){
        Empleado empleadoABorrar = buscarEmpleado(emp2.getDocumento());
        nominaDeEmpleado.remove(empleadoABorrar);
    }

    public void listarEmpleado(){
        System.out.println("+++++++++++++++++++++++++++++++++++++++++");
        for(int indice = 0; indice < nominaDeEmpleado.size(); indice++){     
            System.out.println("Empleado: " + nominaDeEmpleado.get(indice).getApellido() + ", " + nominaDeEmpleado.get(indice).getNombre());
            System.out.println("Documento: " + nominaDeEmpleado.get(indice).getDocumento());
            System.out.println("Cuenta Bancaria Asociada: " + nominaDeEmpleado.get(indice).getClaveBancariaUniforme());
            System.out.println("Categoria: " + nominaDeEmpleado.get(indice).getCategoria().getNombreDeLaCategoria() +
            ", Sueldo base: " + nominaDeEmpleado.get(indice).getCategoria().getSueldoBase()+"\n");
        }
        System.out.println("+++++++++++++++++++++++++++++++++++++++++");
    }


    //agregar, buscar, eliminar y listar ventas
    public void agregarUnaVentaALista(Empleado emp2, String estadoDeVenta, Float precioDeVenta){
        listaDeVenta.add(new Venta(emp2, estadoDeVenta, precioDeVenta));
    }

    public Venta buscarVenta(String _dniDeEmpleadoACargoDeVenta){
        Venta ventaADevolver = null;
        for(Venta _venta : listaDeVenta){
            if(_venta.getEmpleado().getDocumento().equals(_dniDeEmpleadoACargoDeVenta)){
                ventaADevolver = _venta;
            }
        }
        if(ventaADevolver == null){
            throw new VentaInexistenteEnNominaException();
        }
        return ventaADevolver;
    }

    public void removerVentaDeNomina(String _dniDeEmpleadoACargoDeLaVenta){
        Venta ventaARemover = buscarVenta(_dniDeEmpleadoACargoDeLaVenta);
        listaDeVenta.remove(ventaARemover);
    }

    public void listarNominaDeVenta(){
        System.out.println("*****************************************");
        for(int index = 0; index < listaDeVenta.size(); index++){
            System.out.println("Empleado a cargo de la venta: " + listaDeVenta.get(index).getEmpleado().getApellido() + ", " + 
            listaDeVenta.get(index).getEmpleado().getNombre());
            System.out.println("");
            System.out.println("Estado de la venta: " + listaDeVenta.get(index).getEstadoDeLaVenta());
        }
        System.out.println("*****************************************");
    }
    
    

    

}
